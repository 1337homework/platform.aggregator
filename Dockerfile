FROM node:alpine

LABEL authors="Valdas Mazrimas <valdas.mazrimas@gmail.com>"
WORKDIR /srv/aggregator

COPY package*.json ./
RUN npm install --only=production

COPY ./src ./src

EXPOSE 8886

CMD [ "npm", "start" ]

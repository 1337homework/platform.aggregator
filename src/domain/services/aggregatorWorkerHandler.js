const { logger } = require('../../utils/logger')
const { handleError, publishStreamStatus } = require('../../utils/asyncHandlers')
const {
  RECEIVED_INVALID_MESSAGE,
  BINANCE_AGGR_STREAM_COMPLETED,
  BINANCE_AGGR_STREAM_STARTED,
  KRAKEN_AGGR_STREAM_COMPLETED,
  KRAKEN_AGGR_STREAM_STARTED,
  HUOBI_AGGR_STREAM_COMPLETED,
  HUOBI_AGGR_STREAM_STARTED
} = require('../../configuration/constants').reasonCodes
const {
  ROUTING_KEY_STOP_BINANCE,
  ROUTING_KEY_START_BINANCE,
  ROUTING_KEY_STOP_KRAKEN,
  ROUTING_KEY_START_KRAKEN,
  ROUTING_KEY_STOP_HUOBI,
  ROUTING_KEY_START_HUOBI
} = require('../../configuration/constants').communicationTopology
const integrationHandler = require('./socketsIntegrationHandler')

const aggregatorWorkerHandler = (originalMessage) => {
  let message

  try {
    message = JSON.parse(originalMessage.content.toString('utf8'))
  } catch (e) {
    handleError(
      RECEIVED_INVALID_MESSAGE,
      'Message can not be parsed',
      {},
      e,
      originalMessage
    )
    return
  }

  if (
    !message.sagaId ||
    !message.resourceId
  ) {
    handleError(
      RECEIVED_INVALID_MESSAGE,
      'Error in input. Missing resourceId, sagaId or other fields.',
      message,
      originalMessage
    )
    return
  }

  switch (originalMessage.fields.routingKey) {
  case ROUTING_KEY_START_HUOBI:
    logger.log({
      level: 'info',
      message: `Received command to start Huobi worker stream.`
    })
    integrationHandler.connectHuobi()
    publishStreamStatus(HUOBI_AGGR_STREAM_STARTED, message, originalMessage)
    break
  case ROUTING_KEY_STOP_HUOBI:
    logger.log({
      level: 'info',
      message: `Received command to stop Huobi worker stream.`
    })

    integrationHandler.disconnectHuobi()
    publishStreamStatus(HUOBI_AGGR_STREAM_COMPLETED, message, originalMessage)
    break
  case ROUTING_KEY_START_BINANCE:
    logger.log({
      level: 'info',
      message: `Received command to start Binance worker stream.`
    })
    integrationHandler.connectBinance()
    publishStreamStatus(BINANCE_AGGR_STREAM_STARTED, message, originalMessage)
    break
  case ROUTING_KEY_STOP_BINANCE:
    logger.log({
      level: 'info',
      message: `Received command to stop Binance worker stream.`
    })

    integrationHandler.disconnectBinance()
    publishStreamStatus(BINANCE_AGGR_STREAM_COMPLETED, message, originalMessage)
    break
  case ROUTING_KEY_START_KRAKEN:
    logger.log({
      level: 'info',
      message: `Received command to start Kraken worker stream.`
    })

    integrationHandler.connectKraken()
    publishStreamStatus(KRAKEN_AGGR_STREAM_STARTED, message, originalMessage)
    break
  case ROUTING_KEY_STOP_KRAKEN:
    logger.log({
      level: 'info',
      message: `Received command to stop Kraken worker stream.`
    })

    integrationHandler.disconnectKraken()
    publishStreamStatus(KRAKEN_AGGR_STREAM_COMPLETED, message, originalMessage)
    break
  default:
    handleError(
      RECEIVED_INVALID_MESSAGE,
      `Message routing key not recognized: ${originalMessage.fields.routingKey}`,
      message,
      {},
      originalMessage
    )
    break
  }
}

module.exports = {
  aggregatorWorkerHandler
}

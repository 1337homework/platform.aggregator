const WebSocket = require('ws')
const websockets = require('../../interfaces/websockets')
const generalConfig = require('../../configuration/general')
const { DISCONNECTED_FROM_SERVICE } = require('../../configuration/constants').reasonCodes
const { BINANCE, KRAKEN, HUOBI } = require('../../configuration/constants').workers
const { logger } = require('../../utils/logger')
const { handleError } = require('../../utils/asyncHandlers')
const { createEntity } = require('../../utils/entityHandler')

let exchangeWorkers = {
  binance: null,
  huobi: null,
  kraken: null
}

const connect = (type) => {
  const ws = websockets.getServer()

  exchangeWorkers[type] = new WebSocket(generalConfig.workers[type])

  let worker = exchangeWorkers[type]

  worker.on('open', () => {
    logger.log({ level: 'info', message: `Aggregator worker connected to ${type} WS Worker...` })
  })

  worker.on('error', (err) => {
    logger.log({ level: 'error', message: `Exchange worker ${type} stream ws error ${err}` })
    handleError(
      DISCONNECTED_FROM_SERVICE,
      `Error in workers Websocket Service clientId ${ws.clientId}`,
      {},
      err
    )
  })

  worker.on('close', (err) => {
    logger.log({ level: 'info', message: `Exchange worker ${type} closed with ${err}` })
  })

  worker.on('message', (msg) => {
    let data = {}

    try {
      data = createEntity(msg)
    } catch (e) {
      logger.log({ level: 'error', message: `Error creating entity ${e}` })
      data = {}
    }

    ws.broadcast(data)
  })
}

const disconnect = (type) => {
  if (exchangeWorkers[type]) {
    exchangeWorkers[type].close()
  }
}

module.exports = {
  connectHuobi: () => {
    connect(HUOBI)
  },
  disconnectHuobi: () => {
    disconnect(HUOBI)
  },
  connectKraken: () => {
    connect(KRAKEN)
  },
  disconnectKraken: () => {
    disconnect(KRAKEN)
  },
  connectBinance: () => {
    connect(BINANCE)
  },
  disconnectBinance: () => {
    disconnect(BINANCE)
  },
}

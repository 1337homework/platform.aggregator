module.exports = {
  workers: {
    BINANCE: 'binance',
    KRAKEN: 'kraken',
    HUOBI: 'huobi'
  },
  reasonCodes: {
    RECEIVED_INVALID_MESSAGE: 'ReceivedInvalidMessage',
    DISCONNECTED_FROM_SERVICE: 'DisconnectedFromService',
    BINANCE_AGGR_STREAM_COMPLETED: 'BinanceAggregationStreamStopped',
    BINANCE_AGGR_STREAM_STARTED: 'BinanceAggregationStreamStarted',
    KRAKEN_AGGR_STREAM_COMPLETED: 'KrakenAggregationStreamStopped',
    KRAKEN_AGGR_STREAM_STARTED: 'KrakenAggregationStreamStarted',
    HUOBI_AGGR_STREAM_COMPLETED: 'HuobiAggregationStreamStopped',
    HUOBI_AGGR_STREAM_STARTED: 'HuobiAggregationStreamStarted'
  },
  communicationTopology: {
    TOPIC_MARKET_ORDER: 'SagaTaskOrder',
    ROUTING_KEY_UPDATE: 'update',
    ROUTING_KEY_STOP_BINANCE: 'aggregator.stopbinance',
    ROUTING_KEY_START_BINANCE: 'aggregator.startbinance',
    ROUTING_KEY_STOP_KRAKEN: 'aggregator.stopkraken',
    ROUTING_KEY_START_KRAKEN: 'aggregator.startkraken',
    ROUTING_KEY_STOP_HUOBI: 'aggregator.stophuobi',
    ROUTING_KEY_START_HUOBI: 'aggregator.starthuobi',
    QUEUE_AGGREGATOR_HANDLER: 'MarketOrder_AggregatorHandler'
  }
}

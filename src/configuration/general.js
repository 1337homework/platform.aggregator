module.exports = {
  proxyConnStr: process.env.HTTPS_PROXY || null,
  devLogger: process.env.DEV_LOGGER || true,
  workers: {
    kraken: process.env.KRAKEN_WORKER_STREAM || 'ws://krakenworker:8887',
    binance: process.env.BINANCE_WORKER_STREAM || 'ws://binanceworker:8888',
    huobi: process.env.HOUBI_WORKER_STREAM || 'ws://huobiworker:8889',
  },
  wssPort: process.env.PORT || '8886',
  wssHost: process.env.WSS_HOST || 'localhost'
}

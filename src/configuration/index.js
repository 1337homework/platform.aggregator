module.exports = {
  generalConfig: require('./general'),
  constants: require('./constants'),
  rabbit: require('./rabbit')
}

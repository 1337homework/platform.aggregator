const WebSocket = require('ws')
const http = require('http')
const generalConfig = require('../configuration/general')
const { logger } = require('../utils/logger')
const uuid = require('uuid/v4')

let httpServer = null
let wsServer = null
let interval = null

const runSocketServer = () => {
  httpServer = http.createServer()
  wsServer = new WebSocket.Server({ noServer: true })

  logger.log({ level: 'info', message: `WebSocket server is waiting for connections on port ${generalConfig.wssPort}` })

  wsServer.on('connection', (ws) => {
    ws.clientId = uuid()
    ws.isAlive = true
    ws.on('pong', function pong() { this.isAlive = true })
    ws.send(JSON.stringify({
      status: `Client with id ${ws.clientId} connected`
    }))
  })

  wsServer.on('close', (msg) => {
    logger.log({ level: 'info', message: `WebSocket server closed. ${msg}` })
  })

  wsServer.broadcast = (data) => {
    wsServer.clients.forEach((ws) => {
      if (ws.readyState === WebSocket.OPEN) {
        ws.send(JSON.stringify(data))
      }
    })
  }

  interval = setInterval(() => {
    wsServer.clients.forEach((ws) => {
      if (ws.isAlive === false) {
        logger.log({ level: 'info', message: 'Disconnecting inactive conneciton' })
        return ws.terminate()
      }

      ws.isAlive = false
      ws.ping('', false, true)
    })
  }, 10000)

  httpServer.on('upgrade', function upgrade(request, socket, head) {
    wsServer.handleUpgrade(request, socket, head, function done(ws) {
      wsServer.emit('connection', ws, request)
    })
  })

  httpServer.listen(
    generalConfig.wssPort,
    generalConfig.wssHost
  )
}

const closeSocketServer = () => {
  clearInterval(interval)
  interval = null
  wsServer.close()
}

const getServer = () => {
  return wsServer
}

module.exports = {
  getServer,
  closeSocketServer,
  runSocketServer
}

const { BINANCE, KRAKEN, HUOBI } = require('../configuration/constants').workers

const binance = (data) => {
  let result = {}

  result[data.stream.split('@')[0].toUpperCase()] = {
    marketName: data.marketName,
    streamName: data.stream,
    eventTime: data.data.E,
    close: parseFloat(data.data.c),
    open: parseFloat(data.data.o),
    high: parseFloat(data.data.h),
    low: parseFloat(data.data.l),
    volume: parseFloat(data.data.v)
  }

  return result
}

const kraken = (data) => {
  let altPairsMap = {
    'XXBTZUSD': 'BTCUSDT',
    'XETHZUSD': 'ETHUSDT',
    'XXRPZUSD': 'XRPUSDT'
  }
  let result = {}

  for (let pair in data.result) {
    let { c: [close], h: [high], l: [low], v: [volume] } = data.result[pair]
    result[altPairsMap[pair]] = {
      marketName: data.marketName,
      streamName: data.stream || data.marketName,
      eventTime: data.eventTime,
      close: parseFloat(close),
      open: parseFloat(data.result[pair].o),
      high: parseFloat(high),
      low: parseFloat(low),
      volume: parseFloat(volume)
    }
  }

  return result
}

const huobi = (data) => {
  let result = {}

  result[data.ch.split('.')[1].toUpperCase()] = {
    marketName: data.marketName,
    streamName: data.ch,
    eventTime: data.ts,
    close: parseFloat(data.tick.close),
    open: parseFloat(data.tick.open),
    high: parseFloat(data.tick.high),
    low: parseFloat(data.tick.low),
    volume: parseFloat(data.tick.vol)
  }

  return result
}

const createEntity = (msg) => {
  const data = JSON.parse(msg)

  switch (data.marketName) {
  case BINANCE:
    return binance(data)
  case KRAKEN:
    return kraken(data)
  case HUOBI:
    return huobi(data)
  default:
    throw new Error('Worker market name not recognized')
  }
}

module.exports = {
  createEntity
}
